from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import messages_only, startswith_words
from events.Message import Message
import requests

class Summon(JanetProcessor):
    @startswith_words("?rsummon")
    def summon(self):
        reddit_search_url = 'https://reddit.com/search.json' 
        search_string_from_user = " ".join(self.split_text[1:])
        search_params = {'q': 'site:i.redd.it ' + search_string_from_user, 'limit' : 1, 'sort' : 'hot' }
        headers = {'User-agent': 'derek v420.69' }
        summon_response = requests.get(reddit_search_url, params=search_params, headers=headers)
        response_image_url = summon_response.json().get('data').get('children')[0].get('data').get('url')
        self.reply(response_image_url)