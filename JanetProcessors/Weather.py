from JanetProcessors.JanetProcessor import JanetProcessor
from .decorators import startswith_words
import requests


class Weather(JanetProcessor):
    @startswith_words("?weather")
    def process(self):
        weather_url = f"http://wttr.in/~{self.location}"
        params = {
            "0": "",  # only show current weather
            "F": "",  # no follow-link
            "T": "",  # no terminal color codes
        }
        resp = requests.get(weather_url, params=params)
        self.reply(f"```{resp.text}```")

    @property
    def location(self):
        if self.args:
            return "+".join(self.args)
        else:
            return "NYC"
