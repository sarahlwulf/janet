from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import messages_only, i_startswith_words
from events.Message import Message
from log.metrics import Metric
import os, random

from models.User import User


class Rocket(JanetProcessor):
    rocket_words = "?rocket"
    # it's just ./rockets but for ~python~
    rockets_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "rockets")

    def rocket_from_name(self):
        if self.args:
            maybe_rocket_path = os.path.join(self.rockets_path, self.args[0])
            if os.path.exists(maybe_rocket_path):
                return maybe_rocket_path

    @property
    def rockets(self):
        return os.listdir(self.rockets_path)

    @i_startswith_words(rocket_words)
    def process(self):
        if self.cmd and self.cmd == "?rockets":
            self.reply("\n".join(self.rockets))
            return

        rocket_choice = self.rocket_from_name()
        if not rocket_choice:
            rocket_choice = random.choice(self.rockets)

        rocket_file = os.path.join(self.rockets_path, rocket_choice)
        rocket = open(rocket_file).read()

        self.reply(rocket)
        Metric("rockets_summoned", value=1).put()
        return rocket
